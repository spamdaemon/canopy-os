# this makefile fragment defines the standard rules
# that are used to compile, link, and run tests
$(BASE_LIB) : $(BASE_OBJECT_FILES)
	@mkdir -p $(dir $@)
	$(create.module)

$(BASE_DIR)/tests/% : $(OBJECT_DIR)/tests-src/%.o $(BASE_LIB)
	@mkdir -p $(dir $@)
	$(link.test)

$(BASE_DIR)/test-scripts/%.passed : $(BASE_DIR)/test-scripts/% $(BASE_LIB)
	@rm -f $@ $<.failed
	@$< > $<.log 2>&1; \
	if [ $$? -eq 0 ]; then \
	    echo Test $< passed;\
            echo PASSED $< > $@;\
        else\
            echo "Test $< failed";\
            echo "Test $< failed" > $<.failed;\
         fi

$(BASE_DIR)/tests/%.passed : $(BASE_DIR)/tests/% 
	@rm -f $@ $<.failed
	@mkdir -p $(dir $@)
	@$(doTest) > $<.log 2>&1; \
	if [ $$? -eq 0 ]; then \
	    echo Test $< passed;\
            echo PASSED $< > $@;\
        else\
            echo "Test $< failed";\
            echo "Test $< failed" > $<.failed;\
         fi
