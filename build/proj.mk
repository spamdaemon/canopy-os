# this makefile fragment defines the projects upon which the current
# libraries libraries, examples, tests, and binaries.
# projects are all those ones that follow the same build pattern as
# the current project
# 
# the following definitions must be output from this file:
# PROJ_NAMES          : the names of projects in link order!
PROJ_NAMES := timber-logging
PROJ_DIRS := $(addprefix $(abspath $(PROJECT_DIR)/..)/,$(PROJ_NAMES))

# these are optional and will be generated automatically if not defined
# PROJ_LIB_SEARCH_PATH  : the search paths for libraries of each project
# PROJ_LINK_LIBS        : the libraries with which to link (in the proper link order)
# PROJ_INC_SEARCH_PATH  : the search paths for include files
# PROJ_COMPILER_DEFINES : definitions passed to the compiler
PROJ_LIB_SEARCH_PATH := $(addsuffix /lib,$(PROJ_DIRS))
PROJ_LINK_LIBS := $(addprefix -l,$(PROJ_NAMES))
PROJ_INC_SEARCH_PATH := $(addsuffix /lib-src,$(PROJ_DIRS))
PROJ_COMPILER_DEFINES :=

