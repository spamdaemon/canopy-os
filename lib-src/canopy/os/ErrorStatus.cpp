#include <canopy/os/ErrorStatus.h>
#include <canopy/os/SystemError.h>
#include <memory>
#include <iosfwd>
#include <cerrno>

namespace canopy {
   namespace os {

      ErrorStatus::ErrorStatus()
            : _error(errno, ::std::generic_category())
      {

      }

      ErrorStatus::~ErrorStatus()
      {

      }

      ::std::string ErrorStatus::message() const
      {
         return _error.message();
      }

      ::std::string ErrorStatus::formatMessage(const ::std::string& msg) const
      {
         return formatMessage(msg.c_str());
      }

      ::std::string ErrorStatus::formatMessage(const char* msg) const
      {
         ::std::string res = message();
         if (msg) {
            res += " : ";
            res += msg;
         }
         return res;
      }

      ::std::string ErrorStatus::getMessage()
      {
         return ErrorStatus().message();
      }

      ::std::string ErrorStatus::getFormattedMessage(const ::std::string& msg)
      {
         return ErrorStatus().formatMessage(msg.c_str());
      }

      ::std::string ErrorStatus::getFormattedMessage(const char* msg)
      {
         return ErrorStatus().formatMessage(msg);
      }

   }
}
::std::ostream& operator<<(::std::ostream& out, const canopy::os::ErrorStatus& status)
{
   return out << status.message();
}

