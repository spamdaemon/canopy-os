#ifndef _CANOPY_OS_ERRORSTATUS_H
#define _CANOPY_OS_ERRORSTATUS_H

#include <system_error>
#include <string>
#include <iosfwd>

namespace canopy {
   namespace os {
      /**
       * This class wraps the system errno in an object and provides
       * some useful functions for formatting error message
       */
      class ErrorStatus
      {
            /**
             * Create an error status object using the current value of errno.
             */
         public:
            ErrorStatus();

            /**
             * Using the provided system status
             * @param err a system error
             */
         public:
            ErrorStatus(::std::system_error err);

            /**
             * Destructor
             */
         public:
            ~ErrorStatus();

            /**
             * Get the value of this error status object.
             * @return the value of errno
             */
         public:
            inline const ::std::error_code& error() const
            {
               return _error;
            }

            /**
             * Get an error message associated with this is error.
             * @return an implementation defined error message
             */
         public:
            ::std::string message() const;

            /**
             * Format this error status into a string. This function is used
             * with SystemError to format error messages.
             * @param msg a message
             * @return an implementation defined error message
             */
         public:
            ::std::string formatMessage(const ::std::string& msg) const;

            /**
             * Format this error status into a string. This function is used
             * with SystemError to format error messages.
             * @param msg a message
             * @return an implementation defined error message
             */
         public:
            ::std::string formatMessage(const char* msg) const;

            /**
             * Get an error message associated with this is error.
             * @return an implementation defined error message
             */
         public:
            static ::std::string getMessage();

            /**
             * Format this error status into a string. This function is used
             * with SystemError to format error messages.
             * @param msg a message
             * @return an implementation defined error message
             */
         public:
            static ::std::string getFormattedMessage(const ::std::string& msg);

            /**
             * Format this error status into a string. This function is used
             * with SystemError to format error messages.
             * @param msg a message
             * @return an implementation defined error message
             */
         public:
            static ::std::string getFormattedMessage(const char* msg);

            /** The current value of errno */
         private:
            ::std::error_code _error;
      };

   }
}
/**
 * Print a representation of an error status to an output stream.
 * @param out an output stream
 * @param status an error status
 * @return out
 */
::std::ostream& operator<<(::std::ostream& out, const canopy::os::ErrorStatus& status);
#endif
