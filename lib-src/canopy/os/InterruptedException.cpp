#include <canopy/os/InterruptedException.h>
#include <memory>

namespace canopy {
   namespace os {
      InterruptedException::InterruptedException(::std::string msg)
            : ::std::runtime_error(::std::move(msg))
      {
      }

      InterruptedException::InterruptedException()
            : ::std::runtime_error("Interrupted")
      {
      }
      InterruptedException::~InterruptedException()
      {
      }

   }
}
