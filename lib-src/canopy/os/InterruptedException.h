#ifndef _CANOPY_OS_INTERRUPTEDEXCEPTION_H
#define _CANOPY_OS_INTERRUPTEDEXCEPTION_H

#include <stdexcept>
#include <string>

namespace canopy {
   namespace os {
      /**
       * The interrupted exception is thrown by methods that
       * would normally block in the kernel, but which have
       * received a signal or interrupt
       */
      class InterruptedException : public ::std::runtime_error
      {

            /**
             * Create a new interrupted exception
             */
         public:
            InterruptedException();

            /**
             * Create a new interrupted exception
             * @param msg a message
             */
         public:
            InterruptedException(::std::string msg);

            /** Destructor */
         public:
            ~InterruptedException();
      };
   }
}

#endif
