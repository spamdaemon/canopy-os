#include <canopy/os/ResourceUnavailable.h>
#include <memory>

namespace canopy {
   namespace os {
      ResourceUnavailable::ResourceUnavailable(bool again)
            : ::std::runtime_error("Resource Unavailable"), _tryAgain(again)
      {
      }

      ResourceUnavailable::ResourceUnavailable(::std::string msg, bool again)
            : ::std::runtime_error(::std::move(msg)), _tryAgain(again)
      {
      }

      ResourceUnavailable::~ResourceUnavailable()
      {
      }
   }

}

