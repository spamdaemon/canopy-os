#ifndef _CANOPY_OS_RESOURCEUNAVAILABLE_H
#define _CANOPY_OS_RESOURCEUNAVAILABLE_H

#include <stdexcept>

namespace canopy {
   namespace os {
      /**
       * The unsupported exception is thrown by function and methods
       * that are not supported by the version of the operating
       * system.
       */
      class ResourceUnavailable : public ::std::runtime_error
      {

            /**
             * Create a new exception.
             * @param tryAgain if true then the operation that failed should be retried after some time.
             */
         public:
            ResourceUnavailable(bool tryAgain = false);

            /**
             * Create a new unsupported exception
             * @param msg a message
             * @param tryAgain if true then the operation that failed should be retried after some time.
             */
         public:
            ResourceUnavailable(::std::string msg, bool tryAgain = false);

            /** Destructor */
         public:
            ~ResourceUnavailable();

            /**
             * Check if the operation that failed should be tried again after some time.
             * @return true if the operation should be tried again
             */
         public:
            inline bool tryAgain() const
            {
               return _tryAgain;
            }

            /** The try again flag */
         private:
            bool _tryAgain;
      };

   }
}
#endif
