#include <canopy/os/SystemError.h>
#include <memory>

namespace canopy {
   namespace os {
      SystemError::SystemError(const ErrorStatus& s)
            : ::std::system_error(s.error()), _status(s)
      {
      }

      SystemError::SystemError(const ErrorStatus& s, ::std::string msg)
            : ::std::system_error(s.error(), msg), _status(s)
      {
      }

      SystemError::SystemError(::std::string msg)
            : SystemError(ErrorStatus(), msg)
      {
      }

      SystemError::SystemError()
            : SystemError(ErrorStatus(), "System Error")
      {
      }

      SystemError::~SystemError()
      {
      }

   }
}

