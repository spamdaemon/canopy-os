#ifndef _CANOPY_OS_SYSTEMERROR_H
#define _CANOPY_OS_SYSTEMERROR_H

#ifndef _CANOPY_OS_ERRORSTATUS_H
#include <canopy/os/ErrorStatus.h>
#endif

#include <system_error>
#include <string>
namespace canopy {
   namespace os {


      /**
       * The system error is thrown functions or methods that may fail
       * for reasons that are not related to programmer error, but may
       * occur due to other circumstances.
       * For example, trying the delete a non-existent file.
       */
      class SystemError : public ::std::system_error
      {
            /**
             * Create a new system error using the current ErrorStatus.
             */
         public:
            SystemError();

            /**
             * Create a new system error with the specified message and current ErrorStatus
             * @param msg a message
             */
         public:
            SystemError(::std::string msg);

            /**
             * Create a new system error
             * @param status an error status
             */
         public:
            SystemError(const ErrorStatus& status);

            /**
             * Create a new system error.
             * @param status an error status
             * @param msg a message
             */
         public:
            SystemError(const ErrorStatus& status, ::std::string msg);

            /**
             * Destructor
             */
         public:
            ~SystemError();

            /** The error code */
         private:
            const ErrorStatus _status;
      };

   }
}
#endif
