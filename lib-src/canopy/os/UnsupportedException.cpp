#include <canopy/os/UnsupportedException.h>
#include <memory>

namespace canopy {
   namespace os {
      UnsupportedException::UnsupportedException(::std::string msg)
            : ::std::runtime_error(::std::move(msg))
      {
      }

      UnsupportedException::UnsupportedException()
            : ::std::runtime_error("Unsupported")
      {
      }
      UnsupportedException::~UnsupportedException()
      {
      }
   }
}
