#ifndef _CANOPY_OS_UNSUPPORTEDEXCEPTION_H
#define _CANOPY_OS_UNSUPPORTEDEXCEPTION_H

#include <stdexcept>
#include <string>

namespace canopy {
   namespace os {
      /**
       * The unsupported exception is thrown by function and methods
       * that are not supported by the version of the operating
       * system.
       */
      class UnsupportedException : public ::std::runtime_error
      {

            /**
             * Create a new unsupported exception
             */
         public:
            UnsupportedException();

            /**
             * Create a new unsupported exception
             * @param msg a message
             */
         public:
            UnsupportedException(::std::string msg);

            /** Destructor */
         public:
            ~UnsupportedException();
      };

   }
}
#endif
